import Vue from "vue";
const moment = require('moment')
Vue.prototype.$elTablePaging = (item, paging) =>
  (item > paging.page - 2 && item <= paging.page + 3) ||
  (paging.page < 2 && item <= paging.page + 5 - paging.page) ||
  (paging.page < 2 && item <= paging.page + 5 - paging.page) ||
  (paging.page >= paging.totalPages - 2 && item > paging.totalPages - 5);

Vue.prototype.$getCombobox = (data, option = {}) => {
  let combobox = [];
  data.map((res) => {
    if (Object.entries(option).length === 0) {
      combobox.push({
        other: res,
        value: res[Object.keys(res).find((obj) => obj.includes("id"))],
        label: res[Object.keys(res).find((obj) => obj.includes("name"))],
      });
    } else {
      // console.log(option);
      // console.log(res[option.label]);
      // console.log(res[option.value]);
      combobox.push({
        other: res,
        value: res[option.value],
        label: res[option.label],
      });
    }
    // return combobox;
  });
  return combobox;
};

Vue.prototype.$onlyNumber = (evt) => {
  evt = evt ? evt : window.event;
  var charCode = evt.which ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
    evt.preventDefault();
  } else {
    return true;
  }
};

Vue.prototype.$convertTime = (row, col, value, index) => {
  return moment(value).format('DD-MM-YYYY hh:mm:ss');
  // console.log(row);
  // console.log(col);
  // console.log(value);
  // console.log(index);
  // return 123;
};

Vue.prototype.$convertDate = (row, col, value, index) => {
  return moment(value).format('DD-MM-YYYY');
};