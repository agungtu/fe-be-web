import Vue from "vue";
import VueLoadingOverlay from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
const pluginProps = {
  // props
  color: "cyan",
};
const pluginSlots = {
  // props
};
Vue.use(VueLoadingOverlay, pluginProps);
