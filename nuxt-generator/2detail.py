import os, shutil
import json

projectFolder = "/root/react/gen01"

route00 = "import React from 'react';\nimport LoadingOverlay from 'react-loading-overlay';\nimport { Link } from 'react-router-dom';\nimport { toast } from 'react-toastify';\nimport {\nBreadcrumb,\nBreadcrumbItem,\nButton,\nCard,\nCardBody,\nCol,\nForm,\nFormFeedback,\nFormGroup,\nFormText,\nInput,\nLabel,\nRow,\n} from 'reactstrap';\nimport { HttpPost, HttpGet, HttpPut } from '../../../providers/httpClient.jsx';"
route0 = ""
route1a = ""
route2a = ""
route3a = ""
route4a = ""
route5a = ""
route6a = ""
route6c = ""
route8a = ""
route8c = ""

with open('template.json') as f:
    data = json.load(f)

for x in data:
    
    for y in x["subMenu"]:
        orSubMenuName = y["subMenuName"]        
        subMenuName = orSubMenuName.lower().replace(" ","_")
        subMenuNameDetail = subMenuName + "_detail"
        route0 = "class "+subMenuNameDetail+" extends React.Component {\nconstructor(props) {\nsuper(props);\nthis.state = {\nloading: false,\nid: props.match.params.id,\ndata: {\n"+subMenuName+": {},\n},\nvalid: {\n"

        for z in y["detailEntity"]:
            if z["isShow"] == True:
                route1a += z["varServer"]+": true,\n"
                route2a += z["varServer"]+": '',\n"
                if z["type"] == "option":
                    route3a += z["varServer"]+"Selected: '',\n"
                if z["isValidate"] == True:
                    route6a += z["varServer"]+": true,\n"
                    route6c += "if (!event.target."+z["varServer"]+".value) {\nvalid."+z["varServer"]+" = false;\nthis.setState((prevState) => ({\ninvalidMessage: {\n...prevState.invalidMessage,\n"+z["varServer"]+": '"+z["name"]+" Harap Diisi',\n},\n}));\n}\nthis.setState({ valid });\nreturn valid."+z["varServer"]+";\n"
                route8c += "<FormGroup className='required'>\n<Row>\n<Label className='control-label' for='"+z["varServer"]+"' sm='12'>\n"+z["name"]+"\n</Label>\n<Col sm='12'>\n<Input\ntype='text'\nname='"+z["varServer"]+"'\nid='"+z["varServer"]+"'\nplaceholder='Masukkan "+z["name"]+"'\ndefaultValue={this.state.data."+z["varServer"]+"}\ninvalid={!this.state.valid."+z["varServer"]+"}\n/>\n<FormFeedback>\n{this.state.invalidMessage."+z["varServer"]+"}\n</FormFeedback>\n</Col>\n</Row>\n</FormGroup>\n"
            else:
                route8a += "<Input\ntype='hidden'\nname='"+z["varServer"]+"'\nid='"+z["varServer"]+"'\ndefaultValue={\nthis.state.data.accounts ? this.state.data.accounts[0]."+z["varServer"]+": null\n}\n/>\n"
            route4a += "let "+z["varServer"]+" = event.target."+z["varServer"]+".value;\n"
            route5a += z["varServer"]+":"+z["varServer"]+",\n"

        route1b = "},\ninvalidMessage: {\n"
        route2b = "},\n"
        route3b = "mode: props.match.params.id.startsWith('new') ? 'Tambah' : 'Ubah',\n};\nthis.handleSubmit = this.handleSubmit.bind(this);\n}\n"
        route3c = "componentDidMount() {\nif (this.state.id != 'new') {\nthis.getdata();\n}\n}\ngetdata() {\nHttpGet(process.env.API_URL1 + '"+y["urlGet"]+"' + this.state.id).then(\n(res) => {\nthis.setState({ data: res.data });\n}\n);\n}\nhandleSubmit(event) {\nevent.preventDefault();\nif (!this.validate(event)) {\nreturn;\n}\nlet newData = {};"
        route4b = "newData = {\n"
        route5b = "};\nthis.setState({\nloading: true,\n});\nif (this.props.match.params.id == 'new') {\nHttpPost(process.env.API_URL1 + '"+y["urlPost"]+"', newData).then(\n(res) => {\nconsole.log(res);\nthis.props.history.push('/main/"+subMenuName+"');\n},\n() => {\nthis.props.history.push('/main/"+subMenuName+"');\nthis.setState({\nloading: false,\n});\n}\n);\n} else {\nHttpPut(process.env.API_URL1 + '"+y["urlPut"]+"' + this.state.id, newData).then(\n(res) => {\nconsole.log(res);\nthis.props.history.push('/main/"+subMenuName+"');\n},\n() => {\nthis.setState({\nloading: false,\n});\n}\n);\n}\n}\n"
        route6 = "validate(event) {\nconst numberRegex = /([0-9\-\(\)\s]+)/;\nconst emailRegex = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;\nlet valid = {"
        route6b = "};\n"
        route6d = "}\n"
        route7 = "formatDate(date) {\nvar d = new Date(date),\nmonth = '' + (d.getMonth() + 1),\nday = '' + d.getDate(),\nyear = d.getFullYear();\nif (month.length < 2) month = '0' + month;\nif (day.length < 2) day = '0' + day;\nif (date != null) {\nreturn [year, month, day].join('-');\n} else {\nreturn null;\n}\n}\nformatDatePass(date) {\nvar d = new Date(date),\nmonth = '' + (d.getMonth() + 1),\nday = '' + d.getDate(),\nyear = d.getFullYear();\nif (month.length < 2) month = '0' + month;\nif (day.length < 2) day = '0' + day;\nif (date != null) {\nreturn [day, month, year].join('');\n} else {\nreturn null;\n}\n}\n"
        route8 = "render() {\nreturn (\n<div>\n<Card>\n<Row>\n<Col sm='6'>\n<h5\nstyle={{\nmarginTop: '14px',\nmarginLeft: '17px',\ntextTransform: 'uppercase',\n}}\n>\n{this.state.mode} "+orSubMenuName+"\n</h5>\n</Col>\n<Col sm='6'>\n<Breadcrumb className='pull-right'>\n<BreadcrumbItem>Data</BreadcrumbItem>\n\n<BreadcrumbItem>\n<Link to='/main/"+subMenuName+"'>"+orSubMenuName+"</Link>\n</BreadcrumbItem>\n\n<BreadcrumbItem active>\n{this.state.mode} "+orSubMenuName+"\n</BreadcrumbItem>\n</Breadcrumb>\n</Col>\n</Row>\n</Card>\n\n<Card>\n<LoadingOverlay\nactive={this.state.loading}\nspinner\ntext='Harap Menunggu..'\n>\n<CardBody>\n<Form onSubmit={this.handleSubmit}>\n{this.state.loading ? null : ("
        route8b = ")}\n<Row>\n<Col>\n"
        route8d = "</Col>\n</Row>\n<FormText className='control-note'> Wajib Diisi</FormText>\n<hr />\n<FormGroup className='text-right mb-0'>\n<Link to='/main/"+subMenuName+"'>\n<Button type='button' color='secondary'>\nBatal\n</Button>\n</Link>\n<Button color='primary' type='submit' className='ml-1'>\n{this.state.mode}\n</Button>\n</FormGroup>\n</Form>\n</CardBody>\n</LoadingOverlay>\n</Card>\n"
        route9 = "</div>);\n}\n"
        route10 = "}\nexport default "+subMenuNameDetail+";"


print (route00 + route0 + route1a + route1b + route2a + route2b + route3a + route3b + route3c + route4a + route4b + route5a + route5b + route6 + route6a +route6b + route6c + route6d + route7 + route8 + route8a + route8b + route8c + route8d + route9 + route10)


