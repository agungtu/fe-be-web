import os, shutil
import json

projectFolder = "/root/react/gen01"

#step 1, create routing
# f= open(projectFolder+"/src/routes/routing.txt","w+")
# f.write("import user_list from '../views/folder_menu/sub_folder/user_list.jsx';\n")
# f.write("import user_detail from '../views/folder_menu/sub_folder/user_detail.jsx';\n")

route00 = "import Dashboard from '../views/general/dashboard/dashboard.jsx';\nimport MyAccount from '../views/general/my-account/my-account.jsx';\nimport MyApplication from '../views/general/my-application/my-application.jsx';\n"
route0 = ""
route1 = "var ThemeRoutes = [\n{collapse: true,path: '/main/general',name: 'Umum',state: 'generalpages',icon: 'mdi mdi-view-dashboard',child: [\n{path: '/main/general/dashboard',name: 'Dasbor',icon: 'mdi mdi-adjust',component: Dashboard,},\n{path: '/main/general/my-application',name: 'Aplikasiku',icon: 'mdi mdi-adjust',component: MyApplication,modal: 'MyApplication',},\n{path: '/main/general/my-account',name: 'Akunku',icon: 'mdi mdi-adjust',component: MyAccount,},\n],},\n"
route2 = ""
route3 = ""


with open('template.json') as f:
    data = json.load(f)

for x in data:
    orMenuName = x["mainMenu"]
    subMenuName = orMenuName.lower().replace(" ","_")
    route2 += "\n{collapse: true,name: '"+orMenuName+"',icon: 'mdi mdi-view-dashboard',child: ["
    
    for y in x["subMenu"]:
        orSubMenuName = y["subMenuName"]        
        subMenuName = orSubMenuName.lower().replace(" ","_")
        subMenuNameDetail = subMenuName + "_detail"
        route0 += "import "+subMenuName+" from '../views/"+subMenuName+"/"+subMenuName+"/"+subMenuName+".jsx';\n"
        route0 += "import "+subMenuNameDetail+" from '../views/"+subMenuName+"/"+subMenuName+"/"+subMenuNameDetail+".jsx';\n"
        route3 += "\n{path: '/main/"+subMenuName+"/:id',component: "+subMenuNameDetail+",},\n{path: '/main/"+subMenuName+"',name: '"+orSubMenuName+"',icon: 'mdi mdi-adjust',component: "+subMenuName+",}\n"
    
    route2 = route2 + route3 + "]},\n"

route4 = "];\nexport default ThemeRoutes;\n"

print (route00 + route0 + route1 + route2 + route4)


