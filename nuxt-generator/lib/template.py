import imp
from fastapi import APIRouter, Depends, HTTPException, Response, File, UploadFile, Security
from datetime import datetime
from bson.objectid import ObjectId
from passlib.hash import pbkdf2_sha256
import json
from bson import json_util
from typing import List

def getHeader(link):
    result = '<base-header class="pb-6">\n\
      <div class="row align-items-center py-4">\n\
        <div class="col-lg-6 col-7">\n\
          <h6 class="h2 text-white d-inline-block mb-0">{{$route.name}}</h6>\n\
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">\n\
            <route-breadcrumb />\n\
          </nav>\n\
        </div>\n\
        <div class="col-lg-6 col-5 text-right">'
    result += link
    result +='</div>\n\
      </div>\n\
    </base-header>'

    return result

def getTitleHeader(title, subtitle):
    result = '<div class="container-fluid mt--6">\n\
      <div>\n\
        <card class="no-border-card" body-classes="px-0 pb-1" footer-classes="pb-2">\n\
          <template slot="header">\n\
            <h3 class="mb-0">'
    result += title
    result += '</h3>\n\
            <p class="text-sm mb-0">'
    result += subtitle
    result += '</p>\n\
          </template>\n\
          '

    return result

def getTitleFooter():
    result = '</card>\n\
          </div>\n\
    </div>'
    return result