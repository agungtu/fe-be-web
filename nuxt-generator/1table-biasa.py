import os, shutil
import json

projectFolder = "/root/react/gen01"

route00 = "import React from 'react';\nimport { Link } from 'react-router-dom';\nimport ReactTable from 'react-table';\nimport 'react-table/react-table.css';\nimport {\nBreadcrumb,\nBreadcrumbItem,\nButton,\nCard,\nCardBody,\nCol,\nModal,\nModalBody,\nModalFooter,\nModalHeader,\nRow,\n} from 'reactstrap';\nimport { HttpDelete, HttpGet } from '../../../providers/httpClient.jsx';\nimport ReactTooltip from 'react-tooltip';"
route0 = ""
route1 = ""
route2 = ""
route3 = ""
route3a = ""
route3b = ""


with open('template.json') as f:
    data = json.load(f)

for x in data:
    
    for y in x["subMenu"]:
        orSubMenuName = y["subMenuName"]        
        subMenuName = orSubMenuName.lower().replace(" ","_")
        subMenuNameDetail = subMenuName + "_detail"
        route0 = "\nclass "+subMenuName+" extends React.Component {\nconstructor(props) {\nsuper(props);\nthis.state = {\ntable: { data: [] },\nloading: false,\nmodalDelete: '',\n};\nthis.createBreadcrumbList = this.createBreadcrumbList.bind(this);\nthis.getDataTable = this.getDataTable.bind(this);\nthis.toggleModalDelete = this.toggleModalDelete.bind(this);\nthis.handleDelete = this.handleDelete.bind(this);\n}\ncreateBreadcrumbList() {\nreturn this.state.table.config.breadCrumb.map((row, index) => {\nif (!row.url) {\nreturn (\n<BreadcrumbItem active={row.isactive}>{row.label}</BreadcrumbItem>\n);\n} else {\nreturn (\n<BreadcrumbItem active={row.isactive}>\n<Link to={row.url}>{row.label}</Link>\n</BreadcrumbItem>\n);\n}\n});\n}\ncomponentDidMount() {\nthis._tBodyComponent = document.getElementsByClassName('rt-tbody')[0];\nthis._tBodyComponent.addEventListener('scroll', this.handleScroll);\nthis.getData();\n}\n"
        route1 = "\nhandleDelete() {\nHttpDelete(\nprocess.env.API_URL1+'"+y["urlDelete"]+"'+this.state.deleteId\n).then((res) => {\nalert('"+orSubMenuName+" Berhasil Dihapus');\nthis.setState({\nmodalDelete: false,\n});\nthis.getData();\n});\n}\ntoggleModalDelete() {\nthis.setState({ modalDelete: !this.state.modalDelete });\n}"
        route2 = "\ngetData() {\nHttpGet(process.env.API_URL1 + '"+y["urlGetList"]+"?size=10').then((res) => {\nconsole.log(res);\nthis.setState({\ntable: {\ndata: res.data,\n},\n});\n});\n}\ngetDataTable(isExcel) {\nlet arr = this.state.table.data;\nif (isExcel) {\narr = this.state.excelData;\n}\nreturn arr.map((prop, key) => {\nreturn {\n...prop,\nnum: key + 1,\nactions: (\n<div className='text-center'>\n<ReactTooltip id='edit'>{'Edit Data'}</ReactTooltip>\n<ReactTooltip id='delete'>{'Delete Data'}</ReactTooltip>\n<Link to={'/main/"+subMenuName+"/' + prop.id_}>\n<Button\ndata-tip=''\ndata-for='edit'\ncolor='inverse'\nsize='sm'\nround='true'\nicon='true'\n>\n<i className='fa fa-edit' />\n</Button>\n</Link>\n<Button\nonClick={() => {\nthis.setState({\nmodalDelete: !this.state.modalDelete,\ndeleteId: prop.id_,\n});\n}}\ndata-tip=''\ndata-for='delete'\nclassName='ml-1'\ncolor='danger'\nsize='sm'\nround='true'\nicon='true'\n>\n<i className='fa fa-times' />\n</Button>\n</div>\n),\n};\n});\n}"
        route3 = "\nrender() {\nconst tableData = this.getDataTable(false);\nreturn (\n<div>\n<Modal isOpen={this.state.modalDelete} toggle={this.toggleModalDelete}>\n<ModalHeader>Konfirmasi</ModalHeader>\n<ModalBody>Apakah Anda Yakin Ingin Menghapus?</ModalBody>\n<ModalFooter>\n<Button\ntype='button'\nonClick={this.toggleModalDelete}\ncolor='secondary'\n>\nBatal\n</Button>\n<Button type='button' onClick={this.handleDelete} color='danger'>\nHapus\n</Button>\n</ModalFooter>\n</Modal>\n<Card>\n<Row>\n<Col sm='6'>\n<h5\nstyle={{\nmarginTop: '14px',\nmarginLeft: '17px',\ntextTransform: 'uppercase',\n}}\n>\n"+orSubMenuName+"\n</h5>\n</Col>\n<Col sm='6'>\n<Breadcrumb className='pull-right'>\n<BreadcrumbItem>"+orSubMenuName+"</BreadcrumbItem>\n<BreadcrumbItem active>"+orSubMenuName+"</BreadcrumbItem>\n</Breadcrumb>\n</Col>\n</Row>\n</Card>\n<Card>\n<div>\n<CardBody>\n<Link to='/main/"+subMenuName+"/new'>\n<Button color='primary' size='sm' className='mb-4 mr-2'>\n<i className='fa fa-plus' /> Tambah "+orSubMenuName+"\n</Button>\n</Link>\n<ReactTable\ncolumns={[\n{\nHeader: '#',\naccessor: 'num',\nwidth: 80,\nclassName: 'text-center',\nsortable: false,\nfilterable: false,\n},"

        for z in y["tableEntity"]:
            if z["isShow"] == True:
                route3a += "\n{\nHeader: '"+z["name"]+"',\naccessor: '"+z["varServer"]+"',\n},"
        
        route3b = "\n{\nHeader: 'Aksi',\naccessor: 'actions',\nsortable: false,\nfilterable: false,\nwidth: 200,\n},\n]}\ndefaultPageSize={10}\nshowPaginationBottom={true}\nclassName='-striped -highlight'\ndata={tableData}\nstyle={{ height: 500, overflowX: 'auto' }}\nfilterable\nnoDataText='Tidak Ada Data'\nmanual\npages={this.state.pages}\nloading={this.state.loading}\nloadingText='Harap Menunggu..'\n/>\n</CardBody>\n</div>\n</Card>\n</div>\n);\n}"

route4 = "\n}\nexport default "+subMenuName+";"

print (route00 + route0 + route1 + route2 + route3 + route3a + route3b + route4)


